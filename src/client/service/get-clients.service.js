const dynamo = require('ebased/service/storage/dynamo');

module.exports.getAllClientsService = async () => {
    let params = {
        TableName: process.env.CLIENTS_TABLE
    };
    let clients = [];
    let iterate = true;

    let data;
    while (iterate) {
        data = await dynamo.scanTable(params);
        clients = [...clients, ...data.Items]
        //Otra opcion de if
        //data.LastEvaluatedKey ? params.LastEvaluatedKey = data.LastEvaluatedKey : iterate = false;
        if (data.LastEvaluatedKey) {
            params.ExclusiveStartKey = data.LastEvaluatedKey;
        } else {
            iterate = false;
        }
    }
    return { clients: clients };

}