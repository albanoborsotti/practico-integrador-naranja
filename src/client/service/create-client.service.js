const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const CLIENT_NAME = config.get('CLIENTS_TABLE');

module.exports.CreateClientService = async (item) => {
    item.status = 'ACTIVE';
    dynamo.putItem({ TableName: CLIENT_NAME, Item: item });
}