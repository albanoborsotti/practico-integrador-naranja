const dynamo = require('ebased/service/storage/dynamo');

module.exports.GetClientbyDNIService = async (client) => {
    const { dni } = client;
    const params = {
        TableName: process.env.CLIENTS_TABLE,
        Key: { dni: dni }
    }
    const data = await dynamo.getItem(params);

    return data.Item
}