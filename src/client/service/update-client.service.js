const dynamo = require('ebased/service/storage/dynamo');
const CLIENTS_TABLE = process.env.CLIENTS_TABLE

module.exports.UpdateClientService = async (client) => {
    const params = {
        TableName: CLIENTS_TABLE,
        Key: { dni: client.dni },
        UpdateExpression: 'SET',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {},
        ReturnValues: 'ALL_OLD',
    }
    Object.keys(client).forEach(key => {
        if (key != 'dni') {
            params.UpdateExpression += ` #${key}=:${key},`;
            params.ExpressionAttributeNames[`#${key}`] = key;
            params.ExpressionAttributeValues[`:${key}`] = client[key];
        }
    });
    params.UpdateExpression = params.UpdateExpression.slice(0, -1);
    const data = await dynamo.updateItem(params);
    
    return data.Attributes;
}