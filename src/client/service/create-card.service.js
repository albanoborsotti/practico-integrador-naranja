const dynamo = require('ebased/service/storage/dynamo');

const { age } = require('../helper/age.helper')
const { getNumberCard, getExpirationDate, getCCV } = require('../helper/card-generator.helper')

const CLIENTS_TABLE = process.env.CLIENTS_TABLE


module.exports.CreateCardService = async (item) => {
    const params = {
        ExpressionAttributeNames: {
            "#CreditCard": "creditCard"
        },
        ExpressionAttributeValues: {
            ":card": {
                "number": getNumberCard(),
                "expirationDate": getExpirationDate(),
                "ccv": getCCV(),
                "type": age(item.birthdayDate) <= 45 ? 'classic' : 'gold'
            }
        },
        Key: {
            "dni": item.dni
        },
        ReturnValues: "ALL_NEW",
        TableName: CLIENTS_TABLE,
        UpdateExpression: "SET #CreditCard = :card"
    };

    await dynamo.updateItem(params);
}