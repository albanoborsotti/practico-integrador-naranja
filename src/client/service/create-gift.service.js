
const dynamo = require('ebased/service/storage/dynamo');

const { getGift } = require('../helper/gift.helper')
const CLIENTS_TABLE = process.env.CLIENTS_TABLE


module.exports.CreateGiftService = async (item) => {
    var params = {
        ExpressionAttributeNames: {
            "#Gift": "gift"
        },
        ExpressionAttributeValues: {
            ":gift": getGift(item.birthdayDate)
        },
        Key: {
            "dni": item.dni
        },
        ReturnValues: "ALL_NEW",
        TableName: CLIENTS_TABLE,
        UpdateExpression: "SET #Gift = :gift"
    };
    await dynamo.updateItem(params);
}