const dynamo = require('ebased/service/storage/dynamo');
const CLIENTS_TABLE = process.env.CLIENTS_TABLE

module.exports.DeleteClientService = async (client) => {
    const { dni } = client;
    const params = {
        ExpressionAttributeNames: {
            "#status": "status"
        },
        ExpressionAttributeValues: {
            ":status": 'INACTIVE'
        },
        Key: {
            "dni": dni
        },
        ReturnValues: "ALL_NEW",
        TableName: CLIENTS_TABLE,
        UpdateExpression: "SET #status = :status"
    };

    return await dynamo.updateItem(params);
}