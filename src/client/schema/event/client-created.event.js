const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreatedEvent extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CLIENT_CREATED',
            specversion: 'v1.0.0',
            payload: payload,
            meta: meta,
            schema: {
                dni: { type: String, required: true },
                name: { type: String, required: true },
                lastName: { type: String, required: true },
                birthdayDate: { type: String, required: true },
                status: { type: String, required: false },
            },
        })
    }
}

module.exports = { ClientCreatedEvent };