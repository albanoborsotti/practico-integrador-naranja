const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientUpdatedEvent extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CLIENT_UPDATED',
            specversion: 'v1.0.0',
            payload: payload,
            meta: meta,
            schema: {
                dni: { type: String, required: true },
                name: { type: String, required: false },
                lastName: { type: String, required: false },
                birthdayDate: { type: String, required: false },
                status: { type: String, required: false },
            },
        })
    }
}

module.exports = { ClientUpdatedEvent };