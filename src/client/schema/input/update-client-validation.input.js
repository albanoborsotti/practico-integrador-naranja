const { InputValidation } = require('ebased/schema/inputValidation');
const { age } = require('../../helper/age.helper')

class UpdateClientInputSchema extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload: payload,
            source: 'FOOD.UPDATE',
            specversion: 'v1.0.0',
            schema: {
                dni: { type: String, required: true },
                birthdayDate: {
                    type: String, required: false, custom: (value) => {
                        return (age(value) >= 18 && age(value) < 65);
                    }
                },

            },
        });
    }
};

module.exports = UpdateClientInputSchema