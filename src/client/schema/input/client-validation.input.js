const { InputValidation } = require('ebased/schema/inputValidation');
const { age } = require('../../helper/age.helper')
class CreateClientValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CREATE_CLIENT',
            specversion: 'v1.0.0',
            source: meta.source,
            payload: payload,
            schema: {
                dni: { type: String, required: true },
                name: { type: String, required: true },
                lastName: { type: String, required: true },
                birthdayDate: {
                    type: String, required: true, custom: (value) => {
                        return (age(value) >= 18 && age(value) < 65);
                    }
                },
                status: { type: String, required: false },
            },
        })
    }

}

module.exports = { CreateClientValidation };