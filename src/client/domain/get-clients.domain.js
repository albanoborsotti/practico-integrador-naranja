const { getAllClientsService } = require('../service/get-clients.service');

module.exports = async (commandPayload, commandMeta) => {
    const data = await getAllClientsService();
    return {
        body: data
    };
}