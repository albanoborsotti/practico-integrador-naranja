const { GetClientbyDNIService } = require('../service/get-client.service');
const { GetClientInputSchema } = require('../schema/input/client-get-validation.input');

module.exports = async (commandPayload, commandMeta) => {
    const data = await GetClientbyDNIService(commandPayload)
    if (!data) {
        return {
            status: 404,
            body: { status: 'Not Found', statusCode: 404 }
        }
    }
    return { body: data };
}