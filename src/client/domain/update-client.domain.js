const { CreateClientValidation } = require('../schema/input/client-validation.input')
const { UpdateClientService } = require('../service/update-client.service');
const { ClientCreatedService } = require('../service/emit-client-created.service');
const { ClientUpdatedEvent } = require('../schema/event/client-updated.event');

module.exports = async (commandPayload, commandMeta) => {
    new CreateClientValidation(commandPayload, commandMeta);
    const res = await UpdateClientService(commandPayload)
    /**
     * res retorna los atributos anteriores al update con el parametro
     * "ReturnValues: 'ALL_OLD'" y comparo directamente con el payload.
     * Si hubo un cambio de fecha se updatea, con esto evito hacer otra consulta a la base
     */
    if (commandPayload.birthdayDate != res.birthdayDate) {
        await ClientCreatedService(new ClientUpdatedEvent(commandPayload, commandMeta));
    }
    return { body: { status: 'updated' } };
}