const { CreateClientValidation } = require('../schema/input/client-validation.input');
const { ClientCreatedEvent } = require('../schema/event/client-created.event')

const { CreateClientService } = require('../service/create-client.service')
const { ClientCreatedService } = require('../service/emit-client-created.service')

module.exports = async (commandPayload, commandMeta) => {

    new CreateClientValidation(commandPayload, commandMeta)
    //Dynamo Service
    await CreateClientService(commandPayload);

    //SNS Service
    await ClientCreatedService(new ClientCreatedEvent(commandPayload, commandMeta));

    return { status: 200, body: { status: 'Purchase create' } };
}