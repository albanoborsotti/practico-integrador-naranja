
const { CreateCardService } = require('../service/create-card.service');

module.exports = async (batchEventPayload, batchEventMeta) => {
    const body = JSON.parse(batchEventPayload.Message);
    const updateClient = await CreateCardService(body);
    return { body: updateClient };
}