const { CreateGiftService } = require('../service/create-gift.service');

module.exports = async (batchEventPayload, batchEventMeta) => {
    const body = JSON.parse(batchEventPayload.Message);
    const updateClient = await CreateGiftService(body);
    return { body: updateClient };
}