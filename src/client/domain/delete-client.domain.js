
const { DeleteClientService } = require('../service/delete-client.service')

module.exports = async (commandPayload, commandMeta) => {
    const res = await DeleteClientService(commandPayload)
    return { body: { status: 'deleted' } };
}