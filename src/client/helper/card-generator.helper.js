/**
 * 
 * @returns CardNumber
 */

function getNumberCard() {
    return getRandom(10000, 10000) + "-" + getRandom(10000, 10000) + "-" + getRandom(10000, 10000) + "-" + getRandom(10000, 10000);
}

/**
 * 
 * @returns Expiration Date
 */

function getExpirationDate() {
    return String(Math.floor(Math.random() * (12 - 1 + 1)) + 1).padStart(2, '0') + "/" + Math.floor((Math.random() * (36 - 25) + 25));
}

/**
 * 
 * @returns CCV
 */

function getCCV() {
    return getRandom(1000, 1000)
}

/**
 * 
 * @param {number} max 
 * @param {number} min 
 * @returns number in [min,max]
 */

function getRandom(max, min) {
    return (Math.floor(Math.random() * max) + min).toString().substring(1);
}

module.exports = {
    getNumberCard,
    getExpirationDate,
    getCCV
}