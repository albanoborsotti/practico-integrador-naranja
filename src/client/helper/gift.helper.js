/**
 * 
 * @param {*} dateOfBirdth 
 * @returns 
 */

module.exports.getGift = function (dateOfBirdth) {
    const brd = new Date(dateOfBirdth)
    const month = brd.getMonth() + 1;
    const day = brd.getDate();
    if ((month <= 3 && day <= 20) || (month === 12 && day >= 21)) {
        return 'T-shirt';
    } else if (month <= 6 && day < 21) {
        return 'buzo';
    } else if (month <= 9 && day < 21) {
        return 'sweater'
    } else {
        return 'shirt'
    }
}