/**
 * 
 * @param {*} birthday 
 * @returns age
 */

function age(birthday) {
    birthday = new Date(birthday);
    return ((new Date().getTime() - birthday.getTime()) / 31536000000).toFixed(0);
}

module.exports = { age }