
const { calculateDiscounts } = require('../helper/discounts-calculator.helper');
const { UpdatePurchasePriceService } = require('../service/update-purchase-price.service');
const { getPoints } = require('../helper/grant-points.helper');
const { UpdateClientService } = require('../../client/service/update-client.service')

module.exports = async (batchEventPayload, batchEventMeta) => {
    let body = batchEventPayload.payload;
    body.products = await calculateDiscounts(body.products, body.client)
    const res = await UpdatePurchasePriceService(body);

    const client = {
        dni: body.client.dni,
        points: getPoints(body.products, body.client)
    }
    await UpdateClientService(client)


    return { body: res };
}