
const uuid = require('uuid')
const { CreatePurchaseValidation } = require('../schema/input/purchase-validation.input')
const { GetClientbyDNIService } = require('../../client/service/get-client.service');
const { CreatePurchaseService } = require('../service/create-purchase.service');
const { EmitPurcheaseCreated } = require('../service/emit-purchease-created.service');
const { PurchaseCreatedEvent } = require('../schema/event/purchase-created.event')

module.exports = async (commandPayload, commandMeta) => {

    new CreatePurchaseValidation(commandPayload, commandMeta)
    const client = await GetClientbyDNIService(commandPayload)

    if (!client) {
        return {
            status: 404,
            body: {
                status: 'Client not found.'
            }
        }
    }

    if (client.status != 'ACTIVE' || !client.creditCard) {
        return {
            status: 403,
            body: {
                status: 'Forbidden.'
            }
        }
    }

    commandPayload.id = uuid.v4()
    await CreatePurchaseService(commandPayload)

    commandPayload.client = client;
    await EmitPurcheaseCreated(new PurchaseCreatedEvent(commandPayload, commandMeta))

    return { status: 200, body: { status: 'Purchase create' } };
}