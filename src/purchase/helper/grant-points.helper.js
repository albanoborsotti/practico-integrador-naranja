
/**
 * 
 * @param {*} products 
 * @param {*} client 
 * @returns 
 */

function getPoints(products, client) {
    const points = client.points || 0;
    let total = 0;
    for (const product of products) {
        total += product.finalPrice;
    }
    return points + parseInt(total / 200)
}

module.exports = {getPoints}