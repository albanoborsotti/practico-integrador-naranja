/**
 * Calculate discounts according to the type of credit card
 * @param {*} products 
 * @param {*} client 
 * @returns products
 */
async function calculateDiscounts(products, client) {
    
    const discount = client.creditCard.type === 'classic' ? 0.08 : 0.12

    const newArrayProducts = products.map(product => {
        product.finalPrice = product.price + product.price * discount
        return product
    });
    
    return newArrayProducts
}

module.exports = {
    calculateDiscounts
}