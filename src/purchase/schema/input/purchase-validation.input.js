const { InputValidation } = require('ebased/schema/inputValidation');
const { ProductSchema } = require('./product.shema')
class CreatePurchaseValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'PURCHASE.CREATE_PURCHASE',
            specversion: 'v1.0.0',
            source: meta.source,
            payload: payload,
            schema: {
                dni: { type: String, required: true },
                products: {
                    type: ProductSchema, required: true, custom: (value) => {
                        return (value.length > 0);
                    }
                }
            },
        })
    }
}



module.exports = { CreatePurchaseValidation };