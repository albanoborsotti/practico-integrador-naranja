const Schemy = require('schemy');

module.exports.ProductSchema = new Schemy(
    [
        {
            'product': {
                type: String,
                required: true
            },
            'price': {
                type: Number,
                required: true
            }
        }
    ]
);
