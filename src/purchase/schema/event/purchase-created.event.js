const { DownstreamEvent } = require('ebased/schema/downstreamEvent');
const { ProductSchema } = require('../input/product.shema');

class PurchaseCreatedEvent extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'EXCHANGE.EXCHANGE_CREATED',
            specversion: 'v1.0.0',
            payload: payload,
            meta: meta,
            schema: {
                id: { type: String, required: true },
                dni: { type: String, required: true },
                client: { type: Object, required: true },
                products: { type: ProductSchema, required: true }
            },
        })
    }
}

module.exports = { PurchaseCreatedEvent };