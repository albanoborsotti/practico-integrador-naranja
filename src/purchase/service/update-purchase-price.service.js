const dynamo = require('ebased/service/storage/dynamo');

module.exports.UpdatePurchasePriceService = async (item) => {
    var params = {
        ExpressionAttributeNames: {
            "#products": "products"
        },
        ExpressionAttributeValues: {
            ":products": item.products
        },
        Key: {
            "id": item.id
        },
        ReturnValues: "ALL_NEW",
        TableName: process.env.PURCHASE_TABLE,
        UpdateExpression: "SET #products = :products"
    };
    const res = await dynamo.updateItem(params);
    
    return res;
}
