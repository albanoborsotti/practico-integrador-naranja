const sqs = require('ebased/service/downstream/sqs');


module.exports.EmitPurcheaseCreated = async (purchaseCreatedEvent, eventMeta) => {

    const sqsCalculatePriceParams = {
        QueueUrl: process.env.PURCHASE_QUEUE_PRICE,
        MessageBody: purchaseCreatedEvent,
    };
    await sqs.send(sqsCalculatePriceParams, eventMeta);

}