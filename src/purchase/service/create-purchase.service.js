const dynamo = require('ebased/service/storage/dynamo');

module.exports.CreatePurchaseService = async (item) => {
    return await dynamo.putItem({ TableName: process.env.PURCHASE_TABLE, Item: item });
}